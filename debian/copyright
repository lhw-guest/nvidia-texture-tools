Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: NVIDIA Texture Tools
Source: https://github.com/castano/nvidia-texture-tools
Files-Excluded:
 data/
 extern/FreeImage/
 extern/atitc/
 extern/glew/
 extern/gnuwin32/
 project/
Comment:
 Above files are excluded from the tarball as they are not available under
 a dfsg compliant license.

Files: *
Copyright: Copyright (c) 2007 NVIDIA Corporation
License: Expat

Files: debian/*
Copyright: Copyright 2012 Lennart Weller <lhw@ring0.de>
License: Expat

Files: src/nvtt/squish/* src/nvtt/SingleColorLookup.h
Copyright: Copyright (c) 2006 Simon Brown <si@sjbrown.co.uk>
License: Expat

Files: extern/poshlib/posh.*
Copyright: Copyright (c) 2004, Brian Hook
License: BSD-2-clause

Files:
 src/nvcore/TextWriter.cpp
 src/nvcore/Debug.cpp
 src/nvcore/Ptr.h
 src/nvcore/RefCounted.h
 src/nvcore/Timer.h
 src/nvcore/Memory.cpp
 src/nvcore/Memory.h
 src/nvcore/TextWriter.h
 src/nvcore/FileSystem.cpp
 src/nvcore/nvcore.h
 src/nvcore/StdStream.h
 src/nvcore/StrLib.cpp
 src/nvcore/StrLib.h
 src/nvcore/Timer.cpp
 src/nvcore/Array.h
 src/nvcore/ForEach.h
 src/nvcore/Debug.h
 src/nvcore/Stream.h
 src/nvcore/Hash.h
 src/nvcore/DefsVcWin32.h
 src/nvcore/FileSystem.h
 src/nvcore/Utils.h
 src/nvimage/Image.cpp
 src/nvimage/ImageIO.cpp
 src/nvimage/nvimage.h
 src/nvimage/FloatImage.cpp
 src/nvimage/ColorBlock.h
 src/nvimage/ColorSpace.h
 src/nvimage/Filter.h
 src/nvimage/ImageIO.h
 src/nvimage/PsdFile.h
 src/nvimage/KtxFile.h
 src/nvimage/ColorSpace.cpp
 src/nvimage/Image.h
 src/nvimage/Quantize.h
 src/nvimage/Filter.cpp
 src/nvimage/KtxFile.cpp
 src/nvimage/FloatImage.h
 src/nvimage/Quantize.cpp
 src/nvimage/TgaFile.h
 src/nvimage/ColorBlock.cpp
 src/nvmath/Matrix.cpp
 src/nvmath/PackedFloat.h
 src/nvmath/ftoi.h
 src/nvmath/Color.cpp
 src/nvmath/Fitting.cpp
 src/nvmath/SphericalHarmonic.h
 src/nvmath/Box.cpp
 src/nvmath/Plane.cpp
 src/nvmath/SimdVector.h
 src/nvmath/Fitting.h
 src/nvmath/SphericalHarmonic.cpp
 src/nvmath/nvmath.h
 src/nvmath/Box.h
 src/nvmath/Color.h
 src/nvmath/Vector.h
 src/nvmath/Plane.h
 src/nvmath/Vector.cpp
 src/nvmath/PackedFloat.cpp
 src/nvmath/Matrix.h
 src/nvthread/Thread.cpp
 src/nvthread/Thread.h
 src/nvthread/Win32.h
 src/nvthread/Event.h
 src/nvthread/ThreadPool.h
 src/nvthread/Mutex.h
 src/nvthread/Event.cpp
 src/nvthread/nvthread.cpp
 src/nvthread/ParallelFor.h
 src/nvthread/ParallelFor.cpp
 src/nvthread/Mutex.cpp
 src/nvthread/Atomic.h
 src/nvthread/nvthread.h
 src/nvthread/ThreadPool.cpp
Copyright: Ignacio Castano <castanyo@yahoo.es>
License: public-domain
 The license is not specified beyond public-domain.
 The line from the source code is the following:
 "This code is in the public domain -- castanyo@yahoo.es"

Files: extern/stb/*
Copyright: Sean Barrett <sean+github@nothings.org>
Source: https://github.com/nothings/stb
License: public-domain
 These libraries are in the public domain (or the equivalent where that is not
 possible). You can do anything you want with them. You have no legal
 obligation to do anything else, although I appreciate attribution.

Files: extern/skylight/*
Copyright: Copyright (c) 2012, Lukas Hosek and Alexander Wilkie
Source: http://cgg.mff.cuni.cz/projects/SkylightModelling/
License: BSD-3-clause

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY AUTHORS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
  * Redistributions of source code must retain the above copyright
 	 notice, this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright
 	 notice, this list of conditions and the following disclaimer in the
 	 documentation and/or other materials provided with the distribution.
  * None of the names of the contributors may be used to endorse or promote
 	 products derived from this software without specific prior written
 	 permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
